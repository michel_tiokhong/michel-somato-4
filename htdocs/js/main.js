var selection;
var menus = {
    '#accueil' : '#art-accueil',
    '#somato' : '#art-somato',
    '#bienfaits' : '#art-bienfaits',
    '#parcours' : '#art-parcours',
    '#liens' : '#art-liens'
    };

// Fonctions installés lorsque la page est prête
jQuery(document).ready(function($) {
    selection = $('#art-accueil');
    selectedMenu = $('#accueil').parent();
//    selection.removeClass('hidden');

    Object.keys(menus).forEach(function(key) 
    {
        var newSelection = $(menus[key]);
        var menu = $(key).parent();
        menu.on("click", function(obj) {
            selection.addClass('hidden');
            newSelection.removeClass('hidden');
            selectedMenu.removeClass('active');
            menu.addClass('active');
            selection = newSelection;
            selectedMenu = menu;
        });
    });


//    var accueil = $("#art-accueil");
//    $("#accueil").on("click", function(obj) {
//        selection.addClass("hidden");
//        accueil.removeClass("hidden");
//        selection = accueil;
//    });
//
//    var somato = $("#art-somato");
//    $("#somato").on("click", function(obj) {
//        selection.addClass("hidden");
//        somato.removeClass("hidden");
//        selection = somato;
//    });
//
//    var bienfaits = $("#art-bienfaits");
//    $("#bienfaits").on("click", function(obj) {
//        selection.addClass("hidden");
//        bienfaits.removeClass("hidden");
//        selection = bienfaits;
//    });
//    
//    var parcours = $("#art-parcours");
//    $("#parcours").on("click", function(obj) {
//        selection.addClass("hidden");
//        parcours.removeClass("hidden");
//        selection = parcours;
//    });
//    
//    var liens = $("#art-liens");
//    $("#liens").on("click", function(obj) {
//        selection.addClass("hidden");
//        liens.removeClass("hidden");
//        selection = liens;
//    });

});
// Expression régulière pour valider un émail
var emailRegex = /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/;

// Surligne le bloc en cas d'erreur
function error(elem, onoff) {
    if (onoff === true) {
        elem.addClass('error');
    } else {
        elem.removeClass('error');
    }
}
// Indique que l'envoi est en cours de traitement
function busy(elem, onoff) {
    if (onoff === true) {
        submit.hide();
        wait.show();
    } else {
        submit.show();
        wait.hide();
    }
}

function isEmpty(str) {
    return (!str || 0 === str.length);
}

// Valide l'entrée utilisateur
function validate(  ) {
    nameInput = $('#nom');
    emailInput = $('#email');
    msgInput = $('#msg');
    submit = $('#submit');
    wait = $('#wait');
    busy(submit, true);

    var ok = false;
    var nomok = !isEmpty(nameInput.val());
    if (nomok) {
        $("#nom-grp").removeClass("error");
        $("#nom-grp div.alert").addClass("hidden");
    } else {
        $("#nom-grp").addClass("error");
        $("#nom-grp div.alert").removeClass("hidden");
    }
    var emailok = emailRegex.test(emailInput.val());
    if (emailok) {
        $("#email-grp").removeClass("error");
        $("#email-grp div.alert").addClass("hidden");
    } else {
        $("#email-grp").addClass("error");
        $("#email-grp div.alert").removeClass("hidden");
    }
    var msgok = !isEmpty(msgInput.val());
    if (msgok) {
        $("#msg-grp").removeClass("error");
        $("#msg-grp div.alert").addClass("hidden");
    } else {
        $("#msg-grp").addClass("error");
        $("#msg-grp div.alert").removeClass("hidden");
    }

    if (nomok & emailok & msgok) {
        // Soumet l'envoie par ajax au script
        $.post('php/send.php',
                {
                    'name': nameInput.val(),
                    'email': emailInput.val(),
                    'msg': msgInput.val()
                },
        function(response) {
            $('#nl-form').fadeOut(200);
            $('#info-header').fadeOut(200);
            $('#nl-success').fadeIn(200);
            if (response.create === 'ok') {
                $('#nl-success').fadeIn(200);
                $('#info-done').fadeIn(200);
            } else {
                $('#nl-failure').fadeIn(200);
            }
        },
                'json'
                ).error(function() {
            console.log('Failed to add subscriber');
            busy(submit, false);
        });
    } else {
        busy(submit, false);
    }
}